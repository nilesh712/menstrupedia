<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

function getUserIP(){
	$client  = @$_SERVER['HTTP_CLIENT_IP'];
	$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	$remote  = $_SERVER['REMOTE_ADDR'];
	if(filter_var($client, FILTER_VALIDATE_IP)){
		$ip = $client;
	}elseif(filter_var($forward, FILTER_VALIDATE_IP)){
		$ip = $forward;
	}else{
			$ip = $remote;
	}

	return $ip;
}

Route::get('/', function()
{
	session_start();
	if(isset($_COOKIE[md5('user')]) && $_COOKIE[md5('user')] != ""){
		if(UserPayment::where('facebook_id','=',$_COOKIE[md5('user')])->where('machine_id','=',md5(getUserIP()).":".$_COOKIE['PHPSESSID'])->count()){
			return View::make('hello');
		} else {
			return View::make('home');
		}
	} else {
		return View::make('home');
	}
});
Route::get('/login', 'instamojoController@createLink');
Route::get('/thanks', 'instamojoController@thanks');


Route::get('/loggedin', function()
{
	session_start();

	if(UserPayment::where('facebook_id','=',md5(Facebook::getUser()))->count()){
		$payment = UserPayment::where('facebook_id','=',md5(Facebook::getUser()))->first();
		setcookie(md5('user'), md5(Facebook::getUser()), time() + (20 * 365 * 24 * 60 * 60), 'http://'.$_SERVER['HTTP_HOST']);
		$payment->machine_id = md5(getUserIP()).":".$_COOKIE['PHPSESSID'];
		$payment->save();
		return Redirect::to('/');
	} else {
		return View::make('loggedin')->withUser(Facebook::getUser());
	}
});

Route::post('/getAccessCodeVerification', function()
{
	session_start();
	//$email = Input::get('email');
	$accesscode = Input::get('accessCode');
	$facebookID = Input::get('facebookID');


		if(isset($accesscode) && $accesscode != "")
		{
			if(UserPayment::where('payment_id','=',$accesscode)->count()){
				$payment = UserPayment::where('payment_id','=',$accesscode)->first();

				if($payment->facebook_id == "" || $payment->facebook_id === md5($facebookID)){
					$payment->facebook_id = md5($facebookID);
					$payment->save();
					setcookie(md5('user'), md5($facebookID), time() + (20 * 365 * 24 * 60 * 60), 'http://'.$_SERVER['HTTP_HOST']);
					$payment->machine_id = md5(getUserIP()).":".$_COOKIE['PHPSESSID'];
					$payment->save();

					return Redirect::to('/');
				} else {
					Session::flash('message','Access Code already used');
					return Redirect::back();
				}
			} else {
				Session::flash('message','Access Code not found. Please Contact For Support');
				return Redirect::back();
			}
		} else {
			Session::flash('message','Access Code Should Not be Blank');
			return Redirect::back();
		}

});


Route::post('/pay', 'instamojoController@createLink');
Route::get('/pay/thanks', 'instamojoController@thanks');
Route::resource('instamojo', 'instamojoController');

Route::get('/facebookLogin', function() {
	return Redirect::to(Facebook::loginUrl());
});
