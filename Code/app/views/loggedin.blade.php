<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Menstrupedia</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			width: 350px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -100px;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}

		input{
			float:right;
		}
	</style>
</head>
<body>
	<div class="welcome">
		<h1>One Step More.</h1><br><br>
		<form method="post" name="pay_now" action="{{ URL::to('/getAccessCodeVerification') }}" style="width:100%;">
			<input type="hidden" name="facebookID" value="{{ $user }}" />
			<!--<span>Email:</span> <input type="text" name="email" /><br><br>-->
			<span>Payment ID:</span> <input type="text" name="accessCode" /><br><br>
			@if(Session::has('message'))
				<span style="font-size:12px;">{{ Session::get('message') }}</span><br><br>
			@endif
			<input type="submit" name="submit" value="GO" />
		</form>
	</div>
</body>
</html>
