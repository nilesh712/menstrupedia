<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Menstrupedia</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			height: 200px;
			left: 25%;
			margin-top: -100px;
			position: absolute;
			top: 50%;
			width: 50%;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
	<div class="welcome">
		@if($status)
			<h1>{{ $message }}</h1>
			<h2>You are one step away now. Login and get access.</h2>
			<a href="{{ URL::to('/facebookLogin') }}"> Login with facebook </a>
		@else
			<h1>{{ $message }}</h1>
		@endif
	</div>
</body>
</html>
