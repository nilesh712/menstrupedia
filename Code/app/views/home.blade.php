<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Menstrupedia</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			color: #999;
		}

		.welcome {
			height: 200px;
			left: 25%;
			margin-top: -100px;
			position: absolute;
			top: 50%;
			width: 50%;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
	<div class="welcome">
		<h1>Welcome user.</h1>
		<h2>Get lifetime access by paying RS.150 .Click below to pay</h2>
		<form method="post" name="pay_now" action="{{ URL::to('/pay') }}">
			<!-- <span>Email:</span><input type="text" name="email" /><br><br> -->
			<input type="submit" name="submit" value="GO" />
		</form>
		<br/><h2>OR</h2><br/>
		<a href="{{ URL::to('/facebookLogin') }}"> Login with facebook </a>
	</div>
</body>
</html>
