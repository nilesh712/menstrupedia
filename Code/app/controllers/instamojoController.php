<?php
use \Instamojo;

class instamojoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function createLink()
    {
			try {
				$api = new Instamojo('d13f17df9d8a68043bc7cffff9f48679', '480808101c9061559a160e84690d28c9');

				$response = $api->linkCreate(array(
					'title'=>'Hello API',
					'description'=>'Create a new Link easily',
					'unit_price'=> '00.00',
					'base_price'=> '00.00',
					'amount'=> '00.00',
					'fees'=> '0.00',
					'currency'=>'INR',
					'redirect_url'=>'http://letsnurture.co.uk/demo/menstrupedia/public/thanks',
					'webhook_url'=>'http://letsnurture.co.uk/demo/menstrupedia/public/thanks'
				));

				return Redirect::to($response['url']);
			}
			catch (Exception $e) {
			    print('Error: ' . $e->getMessage());
			}

			exit();
	  }

	public function thanks()
    {
			$payment = new UserPayment;
			$payment_id = Input::get('payment_id');

			if(isset($payment_id) || $payment_id != ""){
				if(UserPayment::where('payment_id','=',$payment_id)->count()){
					$message = "Thanks for making payment. if still you get problem then mail on support@menstrupedia.com";
					$status = 1;
				} else {
					$payment->payment_id = Input::get('payment_id');
					$payment->payment_status = Input::get('status');

					if($payment->save()){
						// Mail::send('emails.thanksforpayment', array('payment' => $payment), function($message){
						// 		$email = Input::get('email');
						//     $message->to($email, '')->subject('Thanks For your payment.');
						// });

						$message = "Thanks for making payment. if still you get problem then mail on support@menstrupedia.com";
						$status = 1;
					} else {
						$message = "Something wrong.. Please Try again later.. Please mail on support@menstrupedia.com for support";
						$status = 0;
					}
				}

			} else {
				$message = "Something Wrong.. Please Try again later.. Please mail on support@menstrupedia.com for support";
				$status = 0;
			}

    	return View::make('thanks')->withMessage($message)->withStatus($status);
    }

}
