## custom controllers :

	instamojoController (app/controllers/instamojoController)

## custom classes as below

	Instamojo (app/Classes/Instamojo.php)

## External library for facebook

	"boparaiamrit/facebook": "dev-master"

## vendor code for facebook

	vendor/boparaiamrit

## update class aliases in app/config/app.php

	'Instamojo'		  => 'App\Classes\Instamojo',
	'Facebook' => 'Boparaiamrit\Facebook\FacebookFacade',

## facebook token update link

	app/config/packages/boparaiamrit/facebook/config.php

## model
	
	UserPayment

	

